var express = require('express');
var bodyParser = require('body-parser'); 
var mysql = require('mysql');
var router = express.Router();
var app = express(); 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 

var AMBIENTE =1; //0 Desarrollo 1 Produccion

if (AMBIENTE==0) {
  var mysqlConnection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'testappweb',
    password: '',
      database: 'testapp'   
  });
}else if(AMBIENTE==1){
  var mysqlConnection = mysql.createConnection({
    //host: '169.62.217.189',
    host: '10.73.211.235',
    user: 'Test',
    password: 'RomehHg562435%&bv_jhahTYty676',
      database: 'testapp'   
  });
}


mysqlConnection.connect((err)=>{
  if (!err) {
    console.log('DB conexion exitosa');
  }else{
    console.log('DB conexion fallida \n Error: '+ JSON.stringify(err,undefined,2));
  }
});

app.listen(3001,()=> console.log('Express server corriendo en el puerto 3001'));

//GET CLASES 
app.get('/clase',(req, res) =>{
  var sql = "SELECT * FROM clase";
  mysqlConnection.query(sql, (err, rows, fields)=> {
    if(!err) {
      console.log(rows);
      res.send(rows);
    } else {
      console.log(err);     
    }
  });
});

/* //GET CLASE
app.get('/clase/:id',(req, res) =>{
  var sql = "SELECT * FROM clase WHERE idClase = ?";
  mysqlConnection.query(sql, [req.params.id],(err, rows, fields)=> {
    if(!err) {
      //console.log(rows);
      res.send(rows);
    } else {
      console.log(err);     
    }
  });
}); */
//GET CLASE POR ADMINISTRADOR
app.get('/clase/:id',(req, res) =>{
  var sql = "SELECT c.nombre,c.color,c.icono,c.idClase,c.idMateria,m.materia FROM testapp.clase c INNER JOIN  testapp.administrador_clase ac ON ac.idClase=c.idCLase INNER JOIN  testapp.materia m ON m.idMateria=c.idMateria WHERE ac.idAdministrador=?";
  mysqlConnection.query(sql, [req.params.id],(err, rows, fields)=> {
    if(!err) {
      //console.log(rows);
      res.send(rows);
    } else {
      console.log(err);     
    }
  });
});
//POST EVALUACION  CON ID CLASE
app.post('/evaluacion/:id',(req, res) =>{
  var sql = "SELECT e.idEvaluacion,ce.idClase,e.nombre,e.opcion,e.numero,e.respuesta,e.estado,e.fecha FROM testapp.evaluacion e INNER JOIN testapp.clase_evaluacion ce ON ce.idEvaluacion=e.idEvaluacion WHERE idClase = ?";
  mysqlConnection.query(sql, [req.params.id],(err, rows, fields)=> {
    if(!err) {
      //console.log(rows);
      res.send(rows);
    } else {
      console.log(err);     
    }
  });
});

//POST EVALUACION  CON ID CLASE CON PARAMETROS  
app.post('/evaluacion',(req, res) =>{
  var idClase=req.body.idClase;
  var sql = "SELECT e.idEvaluacion,ce.idClase,e.nombre,e.opcion,e.numero,e.respuesta,e.estado,e.fecha FROM testapp.evaluacion e INNER JOIN testapp.clase_evaluacion ce ON ce.idEvaluacion=e.idEvaluacion WHERE idClase = ?";
  mysqlConnection.query(sql,[idClase],(err, rows, fields)=> {
    if(!err) {
      res.send(rows);
    } else {
      console.log(err);     
    }
  });
});

//POST ALUMNO  CON ID CLASE Y ID EVALUACION
app.post('/alumno_eval/',(req, res) =>{
  var idClase=req.body.idClase;
  var idEvaluacion=req.body.idEvaluacion;
  var sql = "SELECT a.nombre,a.apellido,a.idAlumno,ca.idClase,ce.idEvaluacion,e.opcion,e.numero,e.respuesta FROM testapp.alumno a INNER JOIN testapp.clase_alumno ca ON ca.idAlumno=a.idAlumno INNER JOIN testapp.clase_evaluacion ce ON ce.idClase=ca.idClase INNER JOIN testapp.evaluacion e ON ce.idEvaluacion=e.idEvaluacion WHERE ce.idEvaluacion= ? AND ca.idClase= ?";
  mysqlConnection.query(sql, [idEvaluacion,idClase],(err, rows, fields)=> {
    if(!err) {
      console.log(rows);
      res.send(rows);
    } else {
      console.log(err);     
    }
  });
});

//POST REGISTRO RESPUESTA DE APLICACION
app.post('/create_res',(req, res) =>{

  var sql = "INSERT INTO testapp.aplicacion_evaluacion (idClase, idEvaluacion, idAlumno, nota, respuestaAplicativo, fecha) VALUES (?,?,?,?,?,NOW())";
  var idAlumno = req.body.idAlumno;
  var idClase = req.body.idClase;
  var idEvaluacion = req.body.idEvaluacion;
  var nota = req.body.nota;
  var respuesta = req.body.repuesta;

  mysqlConnection.query(sql, [idClase,idEvaluacion,idAlumno,nota,respuesta],(err, rows, fields)=> {
    if(!err) {
      console.log(rows);
      res.send(rows);
    } else {
      console.log(err);     
    }
  });
});











/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
